## 用户模块

### 发送验证码

#### 简要描述

新用户注册登录,发送验证码

#### 请求URL

    http://{{host}}/user/code

#### 请求方式

POST

#### 请求参数

phone

#### 成功返回示例

    发送验证码成功,验证码:725940

### 登录

#### 简要描述

用户登录

#### 请求URL

    http://{{host}}/user/login

#### 请求方式

POST

#### 成功返回示例

![](file/Snipaste1.png)

### 查询用户详情

#### 简要描述

查询用户详情信息

#### 请求URL

    http://{{host}}/user/info/{id}

#### 请求方式

GET

#### 请求参数

id

### 查询用户

#### 简要描述

根据id查询用户信息用于生成token

#### 请求URL

    http://{{host}}/user/{id}

#### 请求方式

GET

#### 请求参数

id

#### 成功返回示例

    "success": true,
    "data": {
        "id": 1,
        "nickName": "lfk",
        "icon": "/imgs/blogs/blog1.jpg"
    }

### 用户签到

#### 简要描述

使用bitMap实现用于签到

#### 请求URL

    http://{{host}}/user/sign

#### 请求方式

POST

#### 成功返回示例

    {
        "success": true
    }

### 用户签到统计

#### 简要描述

应用BitMap对用户当月签到天数进行统计

#### 请求URL

    http://{{host}}/user/sign/count

#### 请求方式

GET


#### 成功返回示例

    {
    "success": true,
    "data": 1
    }

## 商铺模块

### 查询单个商铺

#### 简要描述

根据id查询商铺信息

#### 请求URL

    http://{{host}}/shop/{id}

#### 请求方式

GET

#### 请求参数

id

#### 成功返回示例

    {
    "success": true,
    "data": {
        "id": 1,
        "name": "105茶餐厅",
        "typeId": 1,
        "images": "https://qcloud.dpfile.com/pc/jiclIsCKmOI2arxKN1Uf0Hx3PucIJH8q0QSz-Z8llzcN56-_QiKuOvyio1OOxsRtFoXqu0G3iT2T27qat3WhLVEuLYk00OmSS1IdNpm8K8sG4JN9RIm2mTKcbLtc2o2vfCF2ubeXzk49OsGrXt_KYDCngOyCwZK-s3fqawWswzk.jpg,https://qcloud.dpfile.com/pc/IOf6VX3qaBgFXFVgp75w-KKJmWZjFc8GXDU8g9bQC6YGCpAmG00QbfT4vCCBj7njuzFvxlbkWx5uwqY2qcjixFEuLYk00OmSS1IdNpm8K8sG4JN9RIm2mTKcbLtc2o2vmIU_8ZGOT1OjpJmLxG6urQ.jpg",
        "area": "大关",
        "address": "金华路锦昌文华苑29号",
        "x": 120.149192,
        "y": 30.316078,
        "avgPrice": 80,
        "sold": 4215,
        "comments": 3035,
        "score": 37,
        "openHours": "10:00-22:00",
        "createTime": "2021-12-22T18:10:39",
        "updateTime": "2022-12-29T17:28:40"
        }
    }

### 查询商铺列表

#### 简要描述

查询商铺列表并缓存进Redis

#### 请求URL

    http://{{host}}/shop-type/list

#### 请求方式

GET


#### 成功返回示例

    {
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "美食",
            "icon": "/types/ms.png",
            "sort": 1
        },
        {
            "id": 2,
            "name": "KTV",
            "icon": "/types/KTV.png",
            "sort": 2
        },
        {
            "id": 3,
            "name": "丽人·美发",
            "icon": "/types/lrmf.png",
            "sort": 3
        },
        {
            "id": 10,
            "name": "美睫·美甲",
            "icon": "/types/mjmj.png",
            "sort": 4
        },
        {
            "id": 5,
            "name": "按摩·足疗",
            "icon": "/types/amzl.png",
            "sort": 5
        },
        {
            "id": 6,
            "name": "美容SPA",
            "icon": "/types/spa.png",
            "sort": 6
        },
        {
            "id": 7,
            "name": "亲子游乐",
            "icon": "/types/qzyl.png",
            "sort": 7
        },
        {
            "id": 8,
            "name": "酒吧",
            "icon": "/types/jiuba.png",
            "sort": 8
        },
        {
            "id": 9,
            "name": "轰趴馆",
            "icon": "/types/hpg.png",
            "sort": 9
        },
        {
            "id": 4,
            "name": "健身运动",
            "icon": "/types/jsyd.png",
            "sort": 10
        }
        ]
    }

### 更新商铺

#### 简要描述

更新商铺信息

#### 请求URL

    http://{{host}}/shop

#### 请求方式

PUT

#### 请求体

shop



### 商铺类型分页查询商铺

#### 简要描述

根据商铺类型分页查询商铺信息

#### 请求URL

    http://{{host}}/shop/of/type

#### 请求方式

GET

#### 请求参数

typeId,current,x,y

### 商铺名称分页查询商铺

#### 简要描述

根据商铺名称关键字分页查询商铺信息

#### 请求URL

    http://{{host}}/shop/of/name

#### 请求方式

GET

#### 请求参数

name,current



## 优惠券模块

### 新增普通券

#### 简要描述

新增普通券

#### 请求URL

    http://{{host}}/voucher

#### 请求方式

POST

#### 请求体

voucher

### 新增秒杀券

#### 简要描述

新增秒杀券

#### 请求URL

    http://{{host}}/voucher/seckill

#### 请求方式

POST

#### 请求体

voucher



### 查询店铺优惠卷

#### 简要描述

查询店铺对应的优惠券列表

#### 请求URL

    http://{{host}}/voucher/list/{shopId}

#### 请求方式

GTE

#### 请求参数

shopId

#### 成功返回示例

    {
    "success": true,
    "data": [
        {
            "id": 1,
            "shopId": 1,
            "title": "50元代金券",
            "subTitle": "周一至周日均可使用",
            "rules": "全场通用\\n无需预约\\n可无限叠加\\不兑现、不找零\\n仅限堂食",
            "payValue": 4750,
            "actualValue": 5000,
            "type": 0
        },
        {
            "id": 10,
            "shopId": 1,
            "title": "0秒100券",
            "subTitle": "全场通用",
            "rules": "gewuhjiqodhbdsmok",
            "payValue": 1,
            "actualValue": 10000,
            "type": 1,
            "stock": 98,
            "beginTime": "2022-12-30T12:00:00",
            "endTime": "2023-01-08T23:00:00"
        }
            ]
    }

### 秒杀优惠券接口1

#### 简要描述

应用Redis中的Stream实现消息队列功能,对高并发情况进行流量削峰

#### 请求URL

    http://{{host}}/voucher-order/seckill/{id}

#### 请求方式

POST

#### 请求参数

id

### 秒杀优惠券接口2

#### 简要描述

应用RabbitMQ实现消息队列功能,对高并发情况进行流量削峰

#### 请求URL

    http://{{host}}/voucher-order/seckillMQ/{id}

#### 请求方式

POST

#### 请求参数

id

## 博客模块

### 发布博客

#### 简要描述

用户发布博客

#### 请求URL

    http://{{host}}/blog

#### 请求方式

POST

#### 请求体

blog

### 博客点赞

#### 简要描述

实现博客点赞功能

#### 请求URL

    http://{{host}}/blog/like/{id}

#### 请求方式

PUT

#### 请求参数

id

### 查询个人博客

#### 简要描述

分页查询用户自己的博客

#### 请求URL

    http://{{host}}/blog/of/me

#### 请求方式

GET

#### 请求参数

current

### 热点博客排序

#### 简要描述
根据当前所有博客点赞数从高到低展示在首页

#### 请求URL

    http://{{host}}/blog/hot

#### 请求方式

GET

### 查询用户博客

#### 简要描述

根据id查询博主的探店笔记

#### 请求URL

    http://{{host}}/of/user

#### 请求方式

GET

#### 请求参数
current,id

### 查询单条博客点赞数

#### 简要描述

查询单条博客点赞数

#### 请求URL

    http://{{host}}/blog/likes/{id}

#### 请求方式

GET

#### 请求参数

id

### 查询博客点赞用户

#### 简要描述

查询最新点赞用户列表

#### 请求URL

    http://{{host}}/blog/likes/{id}

#### 请求方式

PUT

#### 请求参数

id

### 查询关注博主动态

#### 简要描述

查询用户关注的博主推送的Blog信息

#### 请求URL

    http://{{host}}/blog/of/follow

#### 请求方式

GET

#### 请求参数

lastId,offset

## 粉丝模块

### 关注用户

#### 简要描述

关注用户

#### 请求URL

    http://{{host}}/follow/{id}/{isFollow}

#### 请求方式

PUT

#### 请求参数

id,isFollow

### 取消关注用户

#### 简要描述
取消关注用户

#### 请求URL

    http://{{host}}/follow/or/not/{id}

#### 请求方式

GET

#### 请求参数

id


### 查询共同关注用户

#### 简要描述
查询共同关注用户

#### 请求URL

    http://{{host}}/follow/common/{id}

#### 请求方式

GET

#### 请求参数

id
