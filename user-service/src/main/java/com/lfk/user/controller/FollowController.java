package com.lfk.user.controller;


import com.lfk.api.dto.Follow;
import com.lfk.api.dto.Result;
import com.lfk.user.service.IFollowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  粉丝控制层
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@RestController
@RequestMapping("/follow")
public class FollowController {

    @Resource
    private IFollowService iFollowService;

    @PutMapping("/{id}/{isFollow}")
    public Result follow(@PathVariable("id")Long followUserId, @PathVariable("isFollow")Boolean isFollow){
        return iFollowService.follow(followUserId,isFollow);
    }

    @GetMapping("/or/not/{id}")
    public Result isFollow(@PathVariable("id")Long followUserId){
        return iFollowService.isFollow(followUserId);
    }

    @GetMapping("/common/{id}")
    public Result followCommons (@PathVariable("id")Long id){
        return iFollowService.followCommons(id);
    }

    @GetMapping("/query/{id}")
    public List<Follow> queryFollowByIds(@PathVariable("id")Long id){
        return iFollowService.queryFollowByIds(id);
    }
}
