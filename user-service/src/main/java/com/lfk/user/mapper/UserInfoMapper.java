package com.lfk.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfk.api.dto.UserInfo;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
