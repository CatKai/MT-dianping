package com.lfk.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.LoginFormDTO;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.User;
import com.lfk.api.dto.UserDTO;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IUserService extends IService<User> {

    Result sendCode(String phone, HttpSession session);

    Result login(LoginFormDTO loginForm, HttpSession session);

    Result sign();

    Result signCount();

    List<UserDTO> getUser(String idStr, List<Long> ids);
}
