package com.lfk.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Follow;
import com.lfk.api.dto.Result;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IFollowService extends IService<Follow> {

    Result follow(Long followUserId, Boolean isFollow);

    Result isFollow(Long followUserId);

    Result followCommons(Long id);

    List<Follow> queryFollowByIds(Long id);
}
