package com.lfk.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.UserInfo;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IUserInfoService extends IService<UserInfo> {

}
