package com.lfk.user.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfk.api.dto.UserInfo;
import com.lfk.user.mapper.UserInfoMapper;
import com.lfk.user.service.IUserInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

}
