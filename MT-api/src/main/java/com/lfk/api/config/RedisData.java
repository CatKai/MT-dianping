package com.lfk.api.config;

import lombok.Data;

import java.time.LocalDateTime;


/**
 * <p>
 *  Redis数据类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Data
public class RedisData {
    private LocalDateTime expireTime;
    private Object data;
}
