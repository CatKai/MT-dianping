package com.lfk.api.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

public class DefaultFeignConfig {
    @Bean
    public Logger.Level feignLoggerLevel(){
        return Logger.Level.HEADERS;
    }

/*    @Bean
    public RequestInterceptor userInfoRequestInterceptor(){
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                UserDTO user = UserHolder.getUser();
                if(user!=null){
                    requestTemplate.header("user-info",user.getId().toString());
                }
            }
        };
    }*/

}
