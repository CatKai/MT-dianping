package com.lfk.api.config;


import com.lfk.api.dto.UserDTO;

/**
 * <p>
 *  UserDTO类的方法
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public class UserHolder {
    private static final ThreadLocal<UserDTO> tl = new ThreadLocal<>();

    public static void saveUser(UserDTO user){
        tl.set(user);
    }

    public static UserDTO getUser(){
        return tl.get();
    }

    public static void removeUser(){
        tl.remove();
    }


}
