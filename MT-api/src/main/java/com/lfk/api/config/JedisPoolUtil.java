package com.lfk.api.config;

import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolUtil {

    private static volatile JedisPool jedisPool = null;

    /** 构造器私有 */
    private JedisPoolUtil() { }

    /** 获取 JedisPool 的实例对象方法*/
    public static JedisPool getJedisPoolInstance() {
        /**
         * 如果 jedisPoll 为 null，说明 jedisPool 还没有实例对象
         */
        if (jedisPool == null) {
            /**
             * 加锁，防止多线程下争抢资源，同时 new 出多个实例对象
             */
            synchronized (JedisPool.class) {
                /**
                 * 再判断一次 jedisPool 是否真的为 null
                 * 以防在加锁的时候已经有线程 new 了一个实例对象了
                 */
                if (jedisPool == null) {
                    /** 这个类可以对连接池进行配置，如最大连接数等 */
                    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
                    /** 设置 JedisPool 最大连接数 */
                    jedisPoolConfig.setMaxTotal(10);
                    jedisPoolConfig.setMaxWaitMillis(3000);
                    jedisPool = new JedisPool(jedisPoolConfig, "47.97.221.88", 6379,0,"199994");
                }
            }
        }

        /** 返回 JedisPool 实例 */
        return jedisPool;
    }

    /**
     * 释放资源
     */
    public static void release(JedisPool jedisPool, Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }
}


//    private static redis.clients.jedis.JedisPool jedisPool =null;
//
//    static {
//        JedisPoolConfig poolConfig = new JedisPoolConfig();
//
//        poolConfig.setMaxTotal(100);
//        poolConfig.setMaxIdle(20);
//        poolConfig.setMinIdle(5);
//        poolConfig.setMaxWaitMillis(3000);
//
//        jedisPool = new redis.clients.jedis.JedisPool(poolConfig, "47.97.221.88", 6379, 5000);
//    }
//
//
//    public static Jedis getJedisPool(){
//        return  jedisPool.getResource();
//    }




