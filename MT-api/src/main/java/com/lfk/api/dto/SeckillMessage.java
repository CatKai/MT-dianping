package com.lfk.api.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * 秒杀消息封装类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SeckillMessage implements Serializable {
    //消息队列发送一条消息包含的属性
    private Long userId;//userId

    private Long orderId;//orderId

    private Long voucherId;//voucherId


    public SeckillMessage(Long userId, Long orderId, Long voucherId) {
        this.userId = userId;
        this.orderId = orderId;
        this.voucherId = voucherId;
    }
}
