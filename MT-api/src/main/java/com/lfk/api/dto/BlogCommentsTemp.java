package com.lfk.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class BlogCommentsTemp {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    private Long userId;


    /**
     * 回复的内容
     */
    private String content;

    /**
     * 昵称，默认是随机字符
     */
    private String nickName;



}
