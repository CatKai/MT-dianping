package com.lfk.api.client;


import com.lfk.api.dto.Follow;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("user-service")
public interface UserClient {
    @GetMapping("/api/user/getUser")
    List<UserDTO> queryUserLikesByIds(@RequestParam("idStr") String idStr, @RequestParam("ids") List<Long> ids);

    @GetMapping("/api/user/{id}")
    Result queryUserById(@PathVariable("id") Long userId);

    @GetMapping("/api/follow/query/{id}")
    List<Follow> queryFollowByIds(@PathVariable("id")Long id);
}
