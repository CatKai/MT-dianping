package com.lfk.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.ShopType;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IShopTypeService extends IService<ShopType> {

    Result queryTypeLists ();
}
