package com.lfk.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.Shop;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IShopService extends IService<Shop> {
    Result queryById(Long id);

    Result update(Shop shop);

    Result queryShopByType(Integer typeId, Integer current, Double x, Double y);
}
