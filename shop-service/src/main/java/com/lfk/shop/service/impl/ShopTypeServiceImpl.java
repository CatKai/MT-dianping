package com.lfk.shop.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.ShopType;
import com.lfk.shop.mapper.ShopTypeMapper;
import com.lfk.shop.service.IShopTypeService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.lfk.api.config.RedisConstants.CACHE_SHOP_TYPE_KEY;


/**
 * <p>
 *  商铺类型服务实现类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryTypeLists() {//String
        // CACHE_SHOP_TYPE_KEY = "cache:shopType";
        // 1.从 Redis 中查询商铺缓存
        String shopTypeJson = stringRedisTemplate.opsForValue().get(CACHE_SHOP_TYPE_KEY);

        // 2.判断 Redis 中是否存在数据
        if (StrUtil.isNotBlank(shopTypeJson)) {
            // 2.1.存在，则返回
            List<ShopType> shopTypes = JSONUtil.toList(shopTypeJson, ShopType.class);
            return Result.ok(shopTypes);
        }
        // 2.2.Redis 中不存在，则从数据库中查询
        List<ShopType> shopTypes = query().orderByAsc("sort").list();

        // 3.判断数据库中是否存在
        if (shopTypes == null) {
            // 3.1.数据库中也不存在，则返回 false
            return Result.fail("分类不存在！");
        }
        // 3.3.2.1.数据库中存在，则将查询到的信息存入 Redis
        stringRedisTemplate.opsForValue().set(CACHE_SHOP_TYPE_KEY, JSONUtil.toJsonStr(shopTypes));
        // 3.3.2.2.返回
        return Result.ok(shopTypes);
    }


    /*
    @Override
public Result queryShopTypeList() {//List
    String key = CACHE_SHOP_TYPE_KEY; // CACHE_SHOP_TYPE_KEY = "cache:shopType";

    // 1.从 Redis 中查询商铺缓存
    List<String> shopTypeJsonList = stringRedisTemplate.opsForList().range(CACHE_SHOP_TYPE_KEY, 0, -1);

    // 2.判断 Redis 中是否有该缓存
    if (shopTypeJsonList != null && !shopTypeJsonList.isEmpty()) {
        // 2.1.若 Redis 中存在该缓存，则直接返回
        ArrayList<ShopType> shopTypes = new ArrayList<>();
        for (String str : shopTypeJsonList) {
            shopTypes.add(JSONUtil.toBean(str, ShopType.class));
        }
        return Result.ok(shopTypes);
    }
    // 2.2.Redis 中若不存在该数据，则从数据库中查询
    List<ShopType> shopTypes = query().orderByAsc("sort").list();

    // 3.判断数据库中是否存在
    if (shopTypes == null || shopTypes.isEmpty()) {
        // 3.1.数据库中也不存在，则返回 false
        return Result.fail("分类不存在！");
    }

    // 3.3.2.1.数据库中存在，则将查询到的信息存入 Redis
    for (ShopType shopType : shopTypes) {
        stringRedisTemplate.opsForList().rightPushAll(CACHE_SHOP_TYPE_KEY, JSONUtil.toJsonStr(shopType));
    }

    // 3.3.2.2.返回
    return Result.ok(shopTypes);
}
     */
}
