package com.lfk.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfk.api.dto.Shop;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface ShopMapper extends BaseMapper<Shop> {

}
