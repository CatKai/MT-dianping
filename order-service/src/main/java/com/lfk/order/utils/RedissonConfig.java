package com.lfk.order.utils;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * <p>
 *  Redission配置类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Configuration
public class RedissonConfig {

    @Bean
    public RedissonClient redissonClient(){
        // 配置
        Config config = new Config();
        config.useSingleServer().setAddress("redis://47.97.221.88:6379")
                .setPassword("199994");
//        config.useSingleServer().setAddress("redis://192.168.193.100:6379");
        // 创建RedissonClient对象
        return Redisson.create(config);
    }


}
