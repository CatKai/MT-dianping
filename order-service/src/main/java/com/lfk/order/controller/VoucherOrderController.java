package com.lfk.order.controller;


import com.lfk.api.dto.Result;
import com.lfk.order.service.IVoucherOrderService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  优惠券订单控制层
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@RestController
@RequestMapping("/voucher-order")
public class VoucherOrderController {

    @Resource
    private IVoucherOrderService voucherOrderService;


    @PostMapping("seckill/{id}")
    public Result seckillVoucher(@PathVariable("id") Long voucherId) {
        return voucherOrderService.seckillVoucher(voucherId);
    }

    @PostMapping("seckillMq/{id}")
    public Result seckillVoucher1(@PathVariable("id") Long voucherId) {
        return voucherOrderService.seckillVoucherMq(voucherId);
    }

}
