package com.lfk.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.Voucher;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IVoucherService extends IService<Voucher> {

    Result queryVoucherOfShop(Long shopId);

    void addSeckillVoucher(Voucher voucher);
}
