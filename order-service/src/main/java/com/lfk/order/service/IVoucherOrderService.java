package com.lfk.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.VoucherOrder;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IVoucherOrderService extends IService<VoucherOrder> {

    Result seckillVoucher(Long voucherId);

    void createVoucherOrder(VoucherOrder voucherOrder);

    Result seckillVoucherMq(Long voucherId);
}
