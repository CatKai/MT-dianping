package com.lfk.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.SeckillVoucher;

/**
 * <p>
 * 秒杀优惠券表，与优惠券是一对一关系 服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface ISeckillVoucherService extends IService<SeckillVoucher> {

}
