package com.lfk.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfk.api.dto.SeckillVoucher;
import com.lfk.order.mapper.SeckillVoucherMapper;
import com.lfk.order.service.ISeckillVoucherService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀优惠券服务实现类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Service
public class SeckillVoucherServiceImpl extends ServiceImpl<SeckillVoucherMapper, SeckillVoucher> implements ISeckillVoucherService {

}
