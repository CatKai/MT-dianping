package com.lfk.order.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfk.api.dto.SeckillVoucher;


/**
 * <p>
 * 秒杀优惠券表，与优惠券是一对一关系 Mapper 接口
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface SeckillVoucherMapper extends BaseMapper<SeckillVoucher> {

}
