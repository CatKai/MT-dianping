package com.lfk.blog.controller;


import com.lfk.api.dto.BlogComments;
import com.lfk.api.dto.Result;
import com.lfk.blog.service.IBlogCommentsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@RestController
@RequestMapping("/blog-comments")
public class BlogCommentsController {
    @Resource
    private IBlogCommentsService blogCommentsService;

    @GetMapping("/{blogId}")
    public Result queryBlogCommentsById(@PathVariable("blogId") Long blogId) {
        // 根据商铺id查询
        return blogCommentsService.queryBlogCommentsById(blogId);
    }

    @PostMapping("/{blogId}")
    public Result saveBlogComments(@PathVariable("blogId") Long blogId, @RequestBody BlogComments blogComments) {
        blogComments.setParentId(1L);
        blogComments.setAnswerId(1L);
        blogComments.setLiked(0);
        blogComments.setStatus(false);
        blogComments.setBlogId(blogId);
        return blogCommentsService.saveBlogComments(blogComments);
    }
    @DeleteMapping("/{blogCommentsId}")
    public Result deleteBlogComments(@PathVariable("blogCommentsId") Long blogCommentsId) {
        return blogCommentsService.deleteBlogComments(blogCommentsId);
    }

}
