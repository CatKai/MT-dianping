package com.lfk.blog.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lfk.api.dto.Blog;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.UserDTO;
import com.lfk.blog.service.IBlogCommentsService;
import com.lfk.blog.service.IBlogService;
import com.lfk.blog.utils.JedisPoolUtil;
import com.lfk.blog.utils.RedisConstants;
import com.lfk.blog.utils.SystemConstants;
import com.lfk.blog.utils.UserHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  博客控制层
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@RestController
@RequestMapping("/blog")
public class BlogController {

    @Resource
    private IBlogService blogService;
    @Resource
    private IBlogCommentsService blogCommentsService;


    @PostMapping
    public Result saveBlog(@RequestBody Blog blog) {
        return blogService.saveBlog(blog);
    }

    @Transactional
    @DeleteMapping("/delete/{id}")
    public Result deleteBlog(@PathVariable("id")Long id){
        // 获取登录用户
        UserDTO user = UserHolder.getUser();
        List<Blog> res=blogService.query()
                .eq("user_id",user.getId())
                .eq("id",id).list();
        if(res==null||res.isEmpty()){
            return Result.fail("删除失败");
        }
        String imgs=res.get(0).getImages();

        //删图片,后期可以丢到MQ中异步操作
        if(imgs!=null&&!imgs.equals("")) {
            JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance();
            Jedis jedis = jedisPool.getResource();
            for (String img : imgs.split(",")) {
                jedis.srem(RedisConstants.SETMEAL_PIC_RESOURCES, img);
                jedis.srem(RedisConstants.SETMEAL_PIC_DB_RESOURCES, img);
            }
            jedis.close();
        }
        //删评论
        blogCommentsService.deleteBlogCommentsByBlogId(id);
        boolean p=blogService.removeById(id);
        if(!p){
            return Result.fail("删除失败");
        }
        return Result.ok("删除成功");
    }

    @PutMapping("/like/{id}")
    public Result likeBlog(@PathVariable("id") Long id) {
        // 修改点赞数量
        return blogService.likeBlog(id);
    }

    @GetMapping("/of/me")
    public Result queryMyBlog(@RequestParam(value = "current", defaultValue = "1") Integer current) {
        // 获取登录用户
        UserDTO user = UserHolder.getUser();
        // 根据用户查询
        Page<Blog> page = blogService.query()
                .eq("user_id", user.getId()).page(new Page<>(current, SystemConstants.MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Blog> records = page.getRecords();
        return Result.ok(records);
    }

    @GetMapping("/hot")
    public Result queryHotBlog(@RequestParam(value = "current", defaultValue = "1") Integer current) {
        return blogService.queryHotBlog(current);
    }

    @GetMapping("/{id}")
    public Result queryBlogById(@PathVariable("id")Long id){
        return blogService.queryBlogById(id);
    }

    @GetMapping("likes/{id}")
    public Result queryBlogLikes(@PathVariable("id")Long id){
        return blogService.queryBlogLikes(id);
    }

    @GetMapping("/of/user")
    public Result queryBlogByUserId(
            @RequestParam(value = "current", defaultValue = "1") Integer current,
            @RequestParam("id") Long id) {
        // 根据用户查询
        Page<Blog> page = blogService.query()
                .eq("user_id", id).page(new Page<>(current, SystemConstants.MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Blog> records = page.getRecords();
        return Result.ok(records);
    }

    @GetMapping("/of/follow")
    public Result queryBlogOfFollow(
            @RequestParam("lastId")Long max,
            @RequestParam(value = "offset",defaultValue = "0")Integer offset) {
        return blogService.queryBlogOfFollow(max,offset);
    }

}
