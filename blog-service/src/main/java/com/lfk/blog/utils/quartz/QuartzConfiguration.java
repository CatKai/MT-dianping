package com.lfk.blog.utils.quartz;

import org.quartz.Scheduler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.concurrent.Executor;

@Configuration
public class QuartzConfiguration {
    @Bean
    public Scheduler scheduler(){
        return schedulerFactoryBean().getScheduler();
    }
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(){
        SchedulerFactoryBean factoryBean=new SchedulerFactoryBean();
//        factoryBean.setSchedulerName("cluster_scheduler");
//        factoryBean.setDataSource(dataSource);//集群通信
        factoryBean.setApplicationContextSchedulerContextKey("application");
        factoryBean.setTaskExecutor(schedulerThreadPool());
        factoryBean.setStartupDelay(10);
        return factoryBean;
    }

    @Bean
    public Executor schedulerThreadPool(){
        ThreadPoolTaskExecutor executor=new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(5);
        executor.setQueueCapacity(1024);
        return executor;
    }
    /*// 使用jobDetail包装job
    @Bean
    public JobDetail myJobDetail() {
        return JobBuilder.newJob(ClearImgJob.class).withIdentity("ClearImgJob").storeDurably().build();
    }

    // 把jobDetail注册到trigger上去
    @Bean
    public Trigger myJobTrigger() {
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInHours(12).repeatForever();

        return TriggerBuilder.newTrigger()
                .forJob(myJobDetail())
                .withIdentity("myJobTrigger")
                .withSchedule(scheduleBuilder)
                .build();
    }*/

}
