package com.lfk.blog.utils.quartz;


import com.lfk.blog.utils.JedisPoolUtil;
import com.lfk.blog.utils.QiniuUtils;
import com.lfk.blog.utils.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Date;
import java.util.Set;


@Slf4j
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class ClearImgJob extends QuartzJobBean {


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
//        System.out.println("1111111");
        JedisPool jedisPool= JedisPoolUtil.getJedisPoolInstance();
        Jedis jedis=jedisPool.getResource();
        //根据Redis中保存的两个set集合进行差值计算，获得垃圾图片名称集合
        Set<String> set = jedis.sdiff(RedisConstants.SETMEAL_PIC_RESOURCES, RedisConstants.SETMEAL_PIC_DB_RESOURCES);
        jedis.close();
        if (set != null)
        {
            for (String picName : set)
            {
                //删除七牛云服务器上的图片
                String res= QiniuUtils.delete(picName);
                //从Redis集合中删除图片名称
                jedisPool.getResource().srem(RedisConstants.SETMEAL_PIC_RESOURCES, picName);
                log.info("自定义任务执行，清理垃圾图片: " +picName+ "结果: "+res);
            }
        }else{
            log.info(new Date()+"启动定时任务成功,无垃圾图片");
        }
    }


}
