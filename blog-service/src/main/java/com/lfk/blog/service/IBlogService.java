package com.lfk.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.Blog;
import com.lfk.api.dto.Result;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IBlogService extends IService<Blog> {

    Result queryBlogById(Long id);

    Result queryHotBlog(Integer current);

    Result likeBlog(Long id);

    Result queryBlogLikes(Long id);

    Result saveBlog(Blog blog);

    Result queryBlogOfFollow(Long max, Integer offset);
}
