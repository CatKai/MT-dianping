package com.lfk.blog.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lfk.api.dto.BlogComments;
import com.lfk.api.dto.Result;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
public interface IBlogCommentsService extends IService<BlogComments> {
    Result queryBlogCommentsById(Long blogId);

    Result saveBlogComments(BlogComments blogComments);

    Result deleteBlogComments(Long blogCommentsId);

    Result deleteBlogCommentsByBlogId(Long blogId);
}
