package com.lfk.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfk.api.client.UserClient;
import com.lfk.api.dto.BlogComments;
import com.lfk.api.dto.BlogCommentsTemp;
import com.lfk.api.dto.Result;
import com.lfk.api.dto.User;
import com.lfk.blog.mapper.BlogCommentsMapper;
import com.lfk.blog.service.IBlogCommentsService;
import com.lfk.blog.service.IBlogService;
import com.lfk.blog.utils.UserHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Cat凯
 * @since 2023-2-1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BlogCommentsServiceImpl extends ServiceImpl<BlogCommentsMapper, BlogComments> implements IBlogCommentsService {

    @Resource
    private IBlogCommentsService blogCommentsService;
    @Resource
    private IBlogService blogService;
    private final UserClient userClient;



    @Override
    public Result queryBlogCommentsById(Long blogId) {
        List<BlogComments> blogComments= blogCommentsService.query().in("blog_id",blogId).list();
//        if(blogComments.isEmpty()){
//            return Result.fail("当前无评论!");
//        }
        List<BlogCommentsTemp>res=new ArrayList<>();
        for(BlogComments blogComment:blogComments){
            BlogCommentsTemp blogCommentsTemp=new BlogCommentsTemp();
            blogCommentsTemp.setId(blogComment.getId());
            blogCommentsTemp.setUserId(blogComment.getUserId());
            blogCommentsTemp.setContent(blogComment.getContent());
            blogCommentsTemp.setNickName(
                    ((User) (userClient.queryUserById(blogComment.getUserId()).getData())).getNickName());//根据id查用户名
            res.add(blogCommentsTemp);
        }

        return Result.ok(res,(long)res.size());
    }

    @Override
    public Result saveBlogComments(BlogComments blogComments) {
        blogComments.setUserId(UserHolder.getUser().getId());
        if(blogComments.getContent().equals("")){
            return Result.fail("评论不能为空!");
        }
        save(blogComments);
        return Result.ok("评论成功");
    }

    @Override
    public Result deleteBlogComments(Long blogCommentsId) {
        BlogComments blogComments = blogCommentsService.getById(blogCommentsId);
        Long userId=blogComments.getUserId();
        if(!UserHolder.getUser().getId().equals(userId)){
            return Result.fail("删除失败!");
        }
        log.info("删除评论成功"+blogComments.getContent());
        blogCommentsService.removeById(blogCommentsId);
        return Result.ok();
    }

    @Override
    public Result deleteBlogCommentsByBlogId(Long blogId) {
        Long userId= blogService.getById(blogId).getUserId();
        if(!UserHolder.getUser().getId().equals(userId)){
            return Result.fail("删除失败!");
        }
        List<BlogComments>comments=blogCommentsService.query().in("blog_id",blogId).list();
        List<Long>list=comments.stream().map(BlogComments::getId)  // 获取每个对象的id
                .collect(Collectors.toList()); // 将获取的A属性构造成新的集合
        blogCommentsService.removeByIds(list);
        return Result.ok();
    }
}
