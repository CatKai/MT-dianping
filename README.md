# MT-点评项目

## 介绍
一个仿大众点评项目,主要功能模块如下:用户短信登录;优惠券秒杀;发布博客及点赞;好友关注;用户签到等。

主要涉及的技术栈:SpingBoot,SpringCloud,MyBatisPlus,MySQL,Redis,RabbitMQ等。

## 项目上线地址
http://47.97.221.88:81/

## 项目亮点

- 应用 Redis 中的 Stream 数据类型与 RabbitMQ 分别实现高并发情况下的异步秒杀订单的流量削峰;
- 运用 Redis 分布式锁和 Lua 脚本实现分布式锁，规避重复下单和库存不足问题,确保一人一单;
- 优化登录拦截器，对当前全局所有路径进行用户token获取与刷新，防止用户token过期;
- 使用 Redis 对高频访问店铺进行缓存，降低 DB 压力同时提升 90% 的数据查询性能;
- 为方便其他业务后续使用缓存，使用泛型 + 函数式编程实现了通用缓存访问静态方法，并解决了缓存雪崩、缓存穿透等问题;
- 使用 Redis 的 Geo + Hash 数据结构分类存储附近商户，并使用 Geo Search 命令实现高性能商户查询及按距离排序;
- 使用 Redis List 数据结构存储用户点赞信息，并基于 ZSet 实现 TopN 点赞排行;
- 使用 Redis Set 数据结构实现用户关注、共同关注功能（交集）;
- 使用 Redis BitMap 实现用户连续签到统计功能。

## 项目架构
![pic123.png](file%2Fpic123.png)
## 使用说明
参见[API说明文档.md](https://gitee.com/CatKai/MT-dianping/blob/master/API%E8%AF%B4%E6%98%8E%E6%96%87%E6%A1%A3.md)

## 未来改进方向

1. 搭建 Redis 集群, Mysql 集群等

2. 项目代码和架构整体优化

# RPC项目地址
https://gitee.com/CatKai/Rpc